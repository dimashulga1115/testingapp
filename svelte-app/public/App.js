'use strict';

function noop() { }
function run(fn) {
    return fn();
}
function blank_object() {
    return Object.create(null);
}
function run_all(fns) {
    fns.forEach(run);
}
function is_function(thing) {
    return typeof thing === 'function';
}
function safe_not_equal(a, b) {
    return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
}
function subscribe(store, ...callbacks) {
    if (store == null) {
        return noop;
    }
    const unsub = store.subscribe(...callbacks);
    return unsub.unsubscribe ? () => unsub.unsubscribe() : unsub;
}
function get_store_value(store) {
    let value;
    subscribe(store, _ => value = _)();
    return value;
}
function null_to_empty(value) {
    return value == null ? '' : value;
}
function custom_event(type, detail) {
    const e = document.createEvent('CustomEvent');
    e.initCustomEvent(type, false, false, detail);
    return e;
}

let current_component;
function set_current_component(component) {
    current_component = component;
}
function get_current_component() {
    if (!current_component)
        throw new Error(`Function called outside component initialization`);
    return current_component;
}
function onMount(fn) {
    get_current_component().$$.on_mount.push(fn);
}
function onDestroy(fn) {
    get_current_component().$$.on_destroy.push(fn);
}
function createEventDispatcher() {
    const component = get_current_component();
    return (type, detail) => {
        const callbacks = component.$$.callbacks[type];
        if (callbacks) {
            // TODO are there situations where events could be dispatched
            // in a server (non-DOM) environment?
            const event = custom_event(type, detail);
            callbacks.slice().forEach(fn => {
                fn.call(component, event);
            });
        }
    };
}
function setContext(key, context) {
    get_current_component().$$.context.set(key, context);
}
function getContext(key) {
    return get_current_component().$$.context.get(key);
}

// source: https://html.spec.whatwg.org/multipage/indices.html
const boolean_attributes = new Set([
    'allowfullscreen',
    'allowpaymentrequest',
    'async',
    'autofocus',
    'autoplay',
    'checked',
    'controls',
    'default',
    'defer',
    'disabled',
    'formnovalidate',
    'hidden',
    'ismap',
    'loop',
    'multiple',
    'muted',
    'nomodule',
    'novalidate',
    'open',
    'playsinline',
    'readonly',
    'required',
    'reversed',
    'selected'
]);

const invalid_attribute_name_character = /[\s'">/=\u{FDD0}-\u{FDEF}\u{FFFE}\u{FFFF}\u{1FFFE}\u{1FFFF}\u{2FFFE}\u{2FFFF}\u{3FFFE}\u{3FFFF}\u{4FFFE}\u{4FFFF}\u{5FFFE}\u{5FFFF}\u{6FFFE}\u{6FFFF}\u{7FFFE}\u{7FFFF}\u{8FFFE}\u{8FFFF}\u{9FFFE}\u{9FFFF}\u{AFFFE}\u{AFFFF}\u{BFFFE}\u{BFFFF}\u{CFFFE}\u{CFFFF}\u{DFFFE}\u{DFFFF}\u{EFFFE}\u{EFFFF}\u{FFFFE}\u{FFFFF}\u{10FFFE}\u{10FFFF}]/u;
// https://html.spec.whatwg.org/multipage/syntax.html#attributes-2
// https://infra.spec.whatwg.org/#noncharacter
function spread(args, classes_to_add) {
    const attributes = Object.assign({}, ...args);
    if (classes_to_add) {
        if (attributes.class == null) {
            attributes.class = classes_to_add;
        }
        else {
            attributes.class += ' ' + classes_to_add;
        }
    }
    let str = '';
    Object.keys(attributes).forEach(name => {
        if (invalid_attribute_name_character.test(name))
            return;
        const value = attributes[name];
        if (value === true)
            str += " " + name;
        else if (boolean_attributes.has(name.toLowerCase())) {
            if (value)
                str += " " + name;
        }
        else if (value != null) {
            str += ` ${name}="${String(value).replace(/"/g, '&#34;').replace(/'/g, '&#39;')}"`;
        }
    });
    return str;
}
const escaped = {
    '"': '&quot;',
    "'": '&#39;',
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;'
};
function escape(html) {
    return String(html).replace(/["'&<>]/g, match => escaped[match]);
}
function each(items, fn) {
    let str = '';
    for (let i = 0; i < items.length; i += 1) {
        str += fn(items[i], i);
    }
    return str;
}
const missing_component = {
    $$render: () => ''
};
function validate_component(component, name) {
    if (!component || !component.$$render) {
        if (name === 'svelte:component')
            name += ' this={...}';
        throw new Error(`<${name}> is not a valid SSR component. You may need to review your build config to ensure that dependencies are compiled, rather than imported as pre-compiled modules`);
    }
    return component;
}
let on_destroy;
function create_ssr_component(fn) {
    function $$render(result, props, bindings, slots) {
        const parent_component = current_component;
        const $$ = {
            on_destroy,
            context: new Map(parent_component ? parent_component.$$.context : []),
            // these will be immediately discarded
            on_mount: [],
            before_update: [],
            after_update: [],
            callbacks: blank_object()
        };
        set_current_component({ $$ });
        const html = fn(result, props, bindings, slots);
        set_current_component(parent_component);
        return html;
    }
    return {
        render: (props = {}, options = {}) => {
            on_destroy = [];
            const result = { title: '', head: '', css: new Set() };
            const html = $$render(result, props, {}, options);
            run_all(on_destroy);
            return {
                html,
                css: {
                    code: Array.from(result.css).map(css => css.code).join('\n'),
                    map: null // TODO
                },
                head: result.title + result.head
            };
        },
        $$render
    };
}
function add_attribute(name, value, boolean) {
    if (value == null || (boolean && !value))
        return '';
    return ` ${name}${value === true ? '' : `=${typeof value === 'string' ? JSON.stringify(escape(value)) : `"${value}"`}`}`;
}

const subscriber_queue = [];
/**
 * Creates a `Readable` store that allows reading by subscription.
 * @param value initial value
 * @param {StartStopNotifier}start start and stop notifications for subscriptions
 */
function readable(value, start) {
    return {
        subscribe: writable(value, start).subscribe,
    };
}
/**
 * Create a `Writable` store that allows both updating and reading by subscription.
 * @param {*=}value initial value
 * @param {StartStopNotifier=}start start and stop notifications for subscriptions
 */
function writable(value, start = noop) {
    let stop;
    const subscribers = [];
    function set(new_value) {
        if (safe_not_equal(value, new_value)) {
            value = new_value;
            if (stop) { // store is ready
                const run_queue = !subscriber_queue.length;
                for (let i = 0; i < subscribers.length; i += 1) {
                    const s = subscribers[i];
                    s[1]();
                    subscriber_queue.push(s, value);
                }
                if (run_queue) {
                    for (let i = 0; i < subscriber_queue.length; i += 2) {
                        subscriber_queue[i][0](subscriber_queue[i + 1]);
                    }
                    subscriber_queue.length = 0;
                }
            }
        }
    }
    function update(fn) {
        set(fn(value));
    }
    function subscribe(run, invalidate = noop) {
        const subscriber = [run, invalidate];
        subscribers.push(subscriber);
        if (subscribers.length === 1) {
            stop = start(set) || noop;
        }
        run(value);
        return () => {
            const index = subscribers.indexOf(subscriber);
            if (index !== -1) {
                subscribers.splice(index, 1);
            }
            if (subscribers.length === 0) {
                stop();
                stop = null;
            }
        };
    }
    return { set, update, subscribe };
}
function derived(stores, fn, initial_value) {
    const single = !Array.isArray(stores);
    const stores_array = single
        ? [stores]
        : stores;
    const auto = fn.length < 2;
    return readable(initial_value, (set) => {
        let inited = false;
        const values = [];
        let pending = 0;
        let cleanup = noop;
        const sync = () => {
            if (pending) {
                return;
            }
            cleanup();
            const result = fn(single ? values[0] : values, set);
            if (auto) {
                set(result);
            }
            else {
                cleanup = is_function(result) ? result : noop;
            }
        };
        const unsubscribers = stores_array.map((store, i) => subscribe(store, (value) => {
            values[i] = value;
            pending &= ~(1 << i);
            if (inited) {
                sync();
            }
        }, () => {
            pending |= (1 << i);
        }));
        inited = true;
        sync();
        return function stop() {
            run_all(unsubscribers);
            cleanup();
        };
    });
}

const LOCATION = {};
const ROUTER = {};

/**
 * Adapted from https://github.com/reach/router/blob/b60e6dd781d5d3a4bdaaf4de665649c0f6a7e78d/src/lib/history.js
 *
 * https://github.com/reach/router/blob/master/LICENSE
 * */

function getLocation(source) {
  return {
    ...source.location,
    state: source.history.state,
    key: (source.history.state && source.history.state.key) || "initial"
  };
}

function createHistory(source, options) {
  const listeners = [];
  let location = getLocation(source);

  return {
    get location() {
      return location;
    },

    listen(listener) {
      listeners.push(listener);

      const popstateListener = () => {
        location = getLocation(source);
        listener({ location, action: "POP" });
      };

      source.addEventListener("popstate", popstateListener);

      return () => {
        source.removeEventListener("popstate", popstateListener);

        const index = listeners.indexOf(listener);
        listeners.splice(index, 1);
      };
    },

    navigate(to, { state, replace = false } = {}) {
      state = { ...state, key: Date.now() + "" };
      // try...catch iOS Safari limits to 100 pushState calls
      try {
        if (replace) {
          source.history.replaceState(state, null, to);
        } else {
          source.history.pushState(state, null, to);
        }
      } catch (e) {
        source.location[replace ? "replace" : "assign"](to);
      }

      location = getLocation(source);
      listeners.forEach(listener => listener({ location, action: "PUSH" }));
    }
  };
}

// Stores history entries in memory for testing or other platforms like Native
function createMemorySource(initialPathname = "/") {
  let index = 0;
  const stack = [{ pathname: initialPathname, search: "" }];
  const states = [];

  return {
    get location() {
      return stack[index];
    },
    addEventListener(name, fn) {},
    removeEventListener(name, fn) {},
    history: {
      get entries() {
        return stack;
      },
      get index() {
        return index;
      },
      get state() {
        return states[index];
      },
      pushState(state, _, uri) {
        const [pathname, search = ""] = uri.split("?");
        index++;
        stack.push({ pathname, search });
        states.push(state);
      },
      replaceState(state, _, uri) {
        const [pathname, search = ""] = uri.split("?");
        stack[index] = { pathname, search };
        states[index] = state;
      }
    }
  };
}

// Global history uses window.history as the source if available,
// otherwise a memory history
const canUseDOM = Boolean(
  typeof window !== "undefined" &&
    window.document &&
    window.document.createElement
);
const globalHistory = createHistory(canUseDOM ? window : createMemorySource());

/**
 * Adapted from https://github.com/reach/router/blob/b60e6dd781d5d3a4bdaaf4de665649c0f6a7e78d/src/lib/utils.js
 *
 * https://github.com/reach/router/blob/master/LICENSE
 * */

const paramRe = /^:(.+)/;

const SEGMENT_POINTS = 4;
const STATIC_POINTS = 3;
const DYNAMIC_POINTS = 2;
const SPLAT_PENALTY = 1;
const ROOT_POINTS = 1;

/**
 * Check if `string` starts with `search`
 * @param {string} string
 * @param {string} search
 * @return {boolean}
 */
function startsWith(string, search) {
  return string.substr(0, search.length) === search;
}

/**
 * Check if `segment` is a root segment
 * @param {string} segment
 * @return {boolean}
 */
function isRootSegment(segment) {
  return segment === "";
}

/**
 * Check if `segment` is a dynamic segment
 * @param {string} segment
 * @return {boolean}
 */
function isDynamic(segment) {
  return paramRe.test(segment);
}

/**
 * Check if `segment` is a splat
 * @param {string} segment
 * @return {boolean}
 */
function isSplat(segment) {
  return segment[0] === "*";
}

/**
 * Split up the URI into segments delimited by `/`
 * @param {string} uri
 * @return {string[]}
 */
function segmentize(uri) {
  return (
    uri
      // Strip starting/ending `/`
      .replace(/(^\/+|\/+$)/g, "")
      .split("/")
  );
}

/**
 * Strip `str` of potential start and end `/`
 * @param {string} str
 * @return {string}
 */
function stripSlashes(str) {
  return str.replace(/(^\/+|\/+$)/g, "");
}

/**
 * Score a route depending on how its individual segments look
 * @param {object} route
 * @param {number} index
 * @return {object}
 */
function rankRoute(route, index) {
  const score = route.default
    ? 0
    : segmentize(route.path).reduce((score, segment) => {
        score += SEGMENT_POINTS;

        if (isRootSegment(segment)) {
          score += ROOT_POINTS;
        } else if (isDynamic(segment)) {
          score += DYNAMIC_POINTS;
        } else if (isSplat(segment)) {
          score -= SEGMENT_POINTS + SPLAT_PENALTY;
        } else {
          score += STATIC_POINTS;
        }

        return score;
      }, 0);

  return { route, score, index };
}

/**
 * Give a score to all routes and sort them on that
 * @param {object[]} routes
 * @return {object[]}
 */
function rankRoutes(routes) {
  return (
    routes
      .map(rankRoute)
      // If two routes have the exact same score, we go by index instead
      .sort((a, b) =>
        a.score < b.score ? 1 : a.score > b.score ? -1 : a.index - b.index
      )
  );
}

/**
 * Ranks and picks the best route to match. Each segment gets the highest
 * amount of points, then the type of segment gets an additional amount of
 * points where
 *
 *  static > dynamic > splat > root
 *
 * This way we don't have to worry about the order of our routes, let the
 * computers do it.
 *
 * A route looks like this
 *
 *  { path, default, value }
 *
 * And a returned match looks like:
 *
 *  { route, params, uri }
 *
 * @param {object[]} routes
 * @param {string} uri
 * @return {?object}
 */
function pick(routes, uri) {
  let match;
  let default_;

  const [uriPathname] = uri.split("?");
  const uriSegments = segmentize(uriPathname);
  const isRootUri = uriSegments[0] === "";
  const ranked = rankRoutes(routes);

  for (let i = 0, l = ranked.length; i < l; i++) {
    const route = ranked[i].route;
    let missed = false;

    if (route.default) {
      default_ = {
        route,
        params: {},
        uri
      };
      continue;
    }

    const routeSegments = segmentize(route.path);
    const params = {};
    const max = Math.max(uriSegments.length, routeSegments.length);
    let index = 0;

    for (; index < max; index++) {
      const routeSegment = routeSegments[index];
      const uriSegment = uriSegments[index];

      if (routeSegment !== undefined && isSplat(routeSegment)) {
        // Hit a splat, just grab the rest, and return a match
        // uri:   /files/documents/work
        // route: /files/* or /files/*splatname
        const splatName = routeSegment === "*" ? "*" : routeSegment.slice(1);

        params[splatName] = uriSegments
          .slice(index)
          .map(decodeURIComponent)
          .join("/");
        break;
      }

      if (uriSegment === undefined) {
        // URI is shorter than the route, no match
        // uri:   /users
        // route: /users/:userId
        missed = true;
        break;
      }

      let dynamicMatch = paramRe.exec(routeSegment);

      if (dynamicMatch && !isRootUri) {
        const value = decodeURIComponent(uriSegment);
        params[dynamicMatch[1]] = value;
      } else if (routeSegment !== uriSegment) {
        // Current segments don't match, not dynamic, not splat, so no match
        // uri:   /users/123/settings
        // route: /users/:id/profile
        missed = true;
        break;
      }
    }

    if (!missed) {
      match = {
        route,
        params,
        uri: "/" + uriSegments.slice(0, index).join("/")
      };
      break;
    }
  }

  return match || default_ || null;
}

/**
 * Check if the `path` matches the `uri`.
 * @param {string} path
 * @param {string} uri
 * @return {?object}
 */
function match(route, uri) {
  return pick([route], uri);
}

/**
 * Add the query to the pathname if a query is given
 * @param {string} pathname
 * @param {string} [query]
 * @return {string}
 */
function addQuery(pathname, query) {
  return pathname + (query ? `?${query}` : "");
}

/**
 * Resolve URIs as though every path is a directory, no files. Relative URIs
 * in the browser can feel awkward because not only can you be "in a directory",
 * you can be "at a file", too. For example:
 *
 *  browserSpecResolve('foo', '/bar/') => /bar/foo
 *  browserSpecResolve('foo', '/bar') => /foo
 *
 * But on the command line of a file system, it's not as complicated. You can't
 * `cd` from a file, only directories. This way, links have to know less about
 * their current path. To go deeper you can do this:
 *
 *  <Link to="deeper"/>
 *  // instead of
 *  <Link to=`{${props.uri}/deeper}`/>
 *
 * Just like `cd`, if you want to go deeper from the command line, you do this:
 *
 *  cd deeper
 *  # not
 *  cd $(pwd)/deeper
 *
 * By treating every path as a directory, linking to relative paths should
 * require less contextual information and (fingers crossed) be more intuitive.
 * @param {string} to
 * @param {string} base
 * @return {string}
 */
function resolve(to, base) {
  // /foo/bar, /baz/qux => /foo/bar
  if (startsWith(to, "/")) {
    return to;
  }

  const [toPathname, toQuery] = to.split("?");
  const [basePathname] = base.split("?");
  const toSegments = segmentize(toPathname);
  const baseSegments = segmentize(basePathname);

  // ?a=b, /users?b=c => /users?a=b
  if (toSegments[0] === "") {
    return addQuery(basePathname, toQuery);
  }

  // profile, /users/789 => /users/789/profile
  if (!startsWith(toSegments[0], ".")) {
    const pathname = baseSegments.concat(toSegments).join("/");

    return addQuery((basePathname === "/" ? "" : "/") + pathname, toQuery);
  }

  // ./       , /users/123 => /users/123
  // ../      , /users/123 => /users
  // ../..    , /users/123 => /
  // ../../one, /a/b/c/d   => /a/b/one
  // .././one , /a/b/c/d   => /a/b/c/one
  const allSegments = baseSegments.concat(toSegments);
  const segments = [];

  allSegments.forEach(segment => {
    if (segment === "..") {
      segments.pop();
    } else if (segment !== ".") {
      segments.push(segment);
    }
  });

  return addQuery("/" + segments.join("/"), toQuery);
}

/**
 * Combines the `basepath` and the `path` into one path.
 * @param {string} basepath
 * @param {string} path
 */
function combinePaths(basepath, path) {
  return `${stripSlashes(
    path === "/" ? basepath : `${stripSlashes(basepath)}/${stripSlashes(path)}`
  )}/`;
}

/* node_modules/svelte-routing/src/Router.svelte generated by Svelte v3.21.0 */

const Router = create_ssr_component(($$result, $$props, $$bindings, $$slots) => {
	let $base;
	let $location;
	let $routes;
	let { basepath = "/" } = $$props;
	let { url = null } = $$props;
	const locationContext = getContext(LOCATION);
	const routerContext = getContext(ROUTER);
	const routes = writable([]);
	$routes = get_store_value(routes);
	const activeRoute = writable(null);
	let hasActiveRoute = false; // Used in SSR to synchronously set that a Route is active.

	// If locationContext is not set, this is the topmost Router in the tree.
	// If the `url` prop is given we force the location to it.
	const location = locationContext || writable(url ? { pathname: url } : globalHistory.location);

	$location = get_store_value(location);

	// If routerContext is set, the routerBase of the parent Router
	// will be the base for this Router's descendants.
	// If routerContext is not set, the path and resolved uri will both
	// have the value of the basepath prop.
	const base = routerContext
	? routerContext.routerBase
	: writable({ path: basepath, uri: basepath });

	$base = get_store_value(base);

	const routerBase = derived([base, activeRoute], ([base, activeRoute]) => {
		// If there is no activeRoute, the routerBase will be identical to the base.
		if (activeRoute === null) {
			return base;
		}

		const { path: basepath } = base;
		const { route, uri } = activeRoute;

		// Remove the potential /* or /*splatname from
		// the end of the child Routes relative paths.
		const path = route.default
		? basepath
		: route.path.replace(/\*.*$/, "");

		return { path, uri };
	});

	function registerRoute(route) {
		const { path: basepath } = $base;
		let { path } = route;

		// We store the original path in the _path property so we can reuse
		// it when the basepath changes. The only thing that matters is that
		// the route reference is intact, so mutation is fine.
		route._path = path;

		route.path = combinePaths(basepath, path);

		if (typeof window === "undefined") {
			// In SSR we should set the activeRoute immediately if it is a match.
			// If there are more Routes being registered after a match is found,
			// we just skip them.
			if (hasActiveRoute) {
				return;
			}

			const matchingRoute = match(route, $location.pathname);

			if (matchingRoute) {
				activeRoute.set(matchingRoute);
				hasActiveRoute = true;
			}
		} else {
			routes.update(rs => {
				rs.push(route);
				return rs;
			});
		}
	}

	function unregisterRoute(route) {
		routes.update(rs => {
			const index = rs.indexOf(route);
			rs.splice(index, 1);
			return rs;
		});
	}

	if (!locationContext) {
		// The topmost Router in the tree is responsible for updating
		// the location store and supplying it through context.
		onMount(() => {
			const unlisten = globalHistory.listen(history => {
				location.set(history.location);
			});

			return unlisten;
		});

		setContext(LOCATION, location);
	}

	setContext(ROUTER, {
		activeRoute,
		base,
		routerBase,
		registerRoute,
		unregisterRoute
	});

	if ($$props.basepath === void 0 && $$bindings.basepath && basepath !== void 0) $$bindings.basepath(basepath);
	if ($$props.url === void 0 && $$bindings.url && url !== void 0) $$bindings.url(url);
	$base = get_store_value(base);
	$location = get_store_value(location);
	$routes = get_store_value(routes);

	 {
		{
			const { path: basepath } = $base;

			routes.update(rs => {
				rs.forEach(r => r.path = combinePaths(basepath, r._path));
				return rs;
			});
		}
	}

	 {
		{
			const bestMatch = pick($routes, $location.pathname);
			activeRoute.set(bestMatch);
		}
	}

	return `${$$slots.default ? $$slots.default({}) : ``}`;
});

/* node_modules/svelte-routing/src/Route.svelte generated by Svelte v3.21.0 */

const Route = create_ssr_component(($$result, $$props, $$bindings, $$slots) => {
	let $activeRoute;
	let $location;
	let { path = "" } = $$props;
	let { component = null } = $$props;
	const { registerRoute, unregisterRoute, activeRoute } = getContext(ROUTER);
	$activeRoute = get_store_value(activeRoute);
	const location = getContext(LOCATION);
	$location = get_store_value(location);

	const route = {
		path,
		// If no path prop is given, this Route will act as the default Route
		// that is rendered if no other Route in the Router is a match.
		default: path === ""
	};

	let routeParams = {};
	let routeProps = {};
	registerRoute(route);

	// There is no need to unregister Routes in SSR since it will all be
	// thrown away anyway.
	if (typeof window !== "undefined") {
		onDestroy(() => {
			unregisterRoute(route);
		});
	}

	if ($$props.path === void 0 && $$bindings.path && path !== void 0) $$bindings.path(path);
	if ($$props.component === void 0 && $$bindings.component && component !== void 0) $$bindings.component(component);
	$activeRoute = get_store_value(activeRoute);
	$location = get_store_value(location);

	 {
		if ($activeRoute && $activeRoute.route === route) {
			routeParams = $activeRoute.params;
		}
	}

	 {
		{
			const { path, component, ...rest } = $$props;
			routeProps = rest;
		}
	}

	return `${$activeRoute !== null && $activeRoute.route === route
	? `${component !== null
		? `${validate_component(component || missing_component, "svelte:component").$$render($$result, Object.assign({ location: $location }, routeParams, routeProps), {}, {})}`
		: `${$$slots.default
			? $$slots.default({ params: routeParams, location: $location })
			: ``}`}`
	: ``}`;
});

/* node_modules/svelte-routing/src/Link.svelte generated by Svelte v3.21.0 */

const Link = create_ssr_component(($$result, $$props, $$bindings, $$slots) => {
	let $base;
	let $location;
	let { to = "#" } = $$props;
	let { replace = false } = $$props;
	let { state = {} } = $$props;
	let { getProps = () => ({}) } = $$props;
	const { base } = getContext(ROUTER);
	$base = get_store_value(base);
	const location = getContext(LOCATION);
	$location = get_store_value(location);
	const dispatch = createEventDispatcher();
	let href, isPartiallyCurrent, isCurrent, props;

	if ($$props.to === void 0 && $$bindings.to && to !== void 0) $$bindings.to(to);
	if ($$props.replace === void 0 && $$bindings.replace && replace !== void 0) $$bindings.replace(replace);
	if ($$props.state === void 0 && $$bindings.state && state !== void 0) $$bindings.state(state);
	if ($$props.getProps === void 0 && $$bindings.getProps && getProps !== void 0) $$bindings.getProps(getProps);
	$base = get_store_value(base);
	$location = get_store_value(location);
	href = to === "/" ? $base.uri : resolve(to, $base.uri);
	isPartiallyCurrent = startsWith($location.pathname, href);
	isCurrent = href === $location.pathname;
	let ariaCurrent = isCurrent ? "page" : undefined;

	props = getProps({
		location: $location,
		href,
		isPartiallyCurrent,
		isCurrent
	});

	return `<a${spread([{ href: escape(href) }, { "aria-current": escape(ariaCurrent) }, props])}>${$$slots.default ? $$slots.default({}) : ``}</a>`;
});

/* src/CreateTest.svelte generated by Svelte v3.21.0 */

const css = {
	code: "option.yes.svelte-oj2ie5{background-color:hsl(120, 100%, 60%)}option.no.svelte-oj2ie5{background-color:hsl(0, 100%, 60%)\n\t}input.svelte-oj2ie5{display:block;margin:0 0 0.5em 0;width:15em\n\t}textarea.svelte-oj2ie5{width:15em}select.svelte-oj2ie5{float:left;width:18em;margin:0 2em 1em 0}.buttons.svelte-oj2ie5{clear:both}",
	map: "{\"version\":3,\"file\":\"CreateTest.svelte\",\"sources\":[\"CreateTest.svelte\"],\"sourcesContent\":[\"<script>\\n\\tlet testname = \\\"\\\";\\n\\tlet startKey;\\n\\tlet questions = {\\n\\t\\t1 : {text: \\\"What do you choose?\\\", yes: 2, no: 3},\\n\\t\\t2 : {text: \\\"See that if 'yes' was chosen. What's next?\\\", yes: 4, no: ''},\\n\\t\\t3 : {text: \\\"See that if 'no' was chosen. What's next?\\\", yes: '', no: 5},\\n\\t\\t4 : {text: \\\"Another 'yes'. What's next?\\\", yes: '', no:''},\\n\\t\\t5 : {text: \\\"Another 'no'. What's next?\\\", yes: '', no:''},\\n\\t\\t6 : {text: \\\"Wow and now you choose the opposite!\\\", yes: '', no: ''}\\n\\t}\\n\\n\\t$: readyTest = {name: testname, startKey, questions };\\n\\n\\tlet questionKey = '';\\n\\t// let text = '';\\n\\t// let yes = '';\\n\\t// let no = '';\\n\\n\\tlet i = 0;\\n\\tlet newQuestionKey = 0;\\n\\n\\t$: maxQuestionKey = Math.max(...Object.keys(questions))\\n\\t$: newQuestionKey = (maxQuestionKey >= newQuestionKey) ? maxQuestionKey+1 : newQuestionKey;\\n\\n\\t$: selectedQuestionKey = Object.keys(questions)[i]\\n\\t$: selectedQuestion = questions[selectedQuestionKey]\\n\\t$: reset_inputs(selectedQuestionKey, selectedQuestion)\\n\\n\\tfunction reset_inputs(key, question) {\\n\\t\\tquestionKey = key;\\n\\t\\t// text = question.text;\\n\\t\\t// yes = question.yes;\\n\\t\\t// no = question.no;\\n\\t}\\n\\n\\tfunction mark_answers(question, markedKey){\\n\\t\\tif(markedKey == question.yes) return 'yes';\\n\\t\\telse if(markedKey == question.no) return 'no';\\n\\t\\telse return '';\\n\\t}\\n\\n\\tfunction create(){\\n\\t\\tquestions[newQuestionKey] = {text : '', yes: '', no: ''}\\n\\t\\ti = Object.keys(questions).length-1;\\n\\t\\tquestions = questions\\n\\t}\\n\\n\\tfunction update(){\\n\\t\\tquestions[selectedQuestionKey].text = text\\n\\t\\tquestions[selectedQuestionKey].yes = yes\\n\\t\\tquestions[selectedQuestionKey].no = no\\n\\t\\tquestions = questions\\n\\t}\\n\\n\\tfunction remove(){\\n\\t\\tif(Object.keys(questions).length > 1){\\n\\t\\t\\tdelete questions[selectedQuestionKey]\\n\\t\\t\\ti = i == Object.keys(questions).length ? i-1 : i\\n\\t\\t\\tquestions = questions\\n\\t\\t}\\n\\t}\\n</script>\\n\\n<style>\\n\\t\\n\\toption.yes {\\n\\t\\tbackground-color: hsl(120, 100%, 60%);\\n\\t}\\n\\toption.no {\\n\\t\\tbackground-color: hsl(0, 100%, 60%)\\n\\t}\\n\\tinput {\\n\\t\\tdisplay:block;\\n\\t\\tmargin: 0 0 0.5em 0;\\n\\t\\twidth: 15em\\n\\t}\\n\\ttextarea {\\n\\t\\twidth: 15em;\\n\\t}\\n\\tselect {\\n\\t\\tfloat:left;\\n\\t\\twidth: 18em;\\n\\t\\tmargin: 0 2em 1em 0;\\n\\t}\\n\\t.buttons {\\n\\t\\tclear: both;\\n\\t}\\n</style>\\n\\n<div style=\\\"display: inline-block\\\">\\n\\t<label><input bind:value=\\\"{testname}\\\" placeholder=\\\"Test name\\\"></label>\\n\\t<p>Start question is </p>\\n\\t<select bind:value=\\\"{startKey}\\\">\\n\\t\\t{#each Object.entries(questions) as [key, question], i}\\n\\t\\t\\t<option value=\\\"{key}\\\"> {key}: {question.text} </option>\\n\\t\\t{/each}\\n\\t</select>\\n</div>\\n\\n<div >\\n\\t<select bind:value={i} size={10}>\\n\\t\\t{#each Object.entries(questions) as [key, question], i}\\n\\t\\t\\t<option class=\\\"{mark_answers(selectedQuestion,key)}\\\" value={i}>\\n\\t\\t\\t\\t({i}) {key} : {question.text}\\n\\t\\t\\t</option>\\n\\t\\t{/each}\\n\\t</select>\\n</div>\\n\\n<div>\\n\\t<p>Question key = {questionKey}</p>\\n\\t<label><textarea rows=4 bind:value={selectedQuestion.text} placeholder=\\\"question text\\\"></textarea></label>\\n\\t<label><input bind:value={selectedQuestion.yes} placeholder=\\\"Next question key on 'yes'\\\"></label>\\n\\t<label><input bind:value={selectedQuestion.no} placeholder=\\\"Next question key on 'no'\\\"></label>\\n</div>\\n\\n\\n\\n<div class=\\\"buttons\\\">\\n\\t<button on:click=\\\"{create}\\\">Create</button>\\n\\t<button on:click=\\\"{update}\\\">Update</button>\\n\\t<button on:click=\\\"{remove}\\\">Delete</button>\\n</div>\\n\\n<p>To save test copy that JSON:<br>\\n\\t<!-- New question Key = {newQuestionKey}<br> -->\\n\\t{JSON.stringify(readyTest)}<br>\\n\\tand put it into store.js\\n</p>\\n\"],\"names\":[],\"mappings\":\"AAkEC,MAAM,IAAI,cAAC,CAAC,AACX,gBAAgB,CAAE,IAAI,GAAG,CAAC,CAAC,IAAI,CAAC,CAAC,GAAG,CAAC,AACtC,CAAC,AACD,MAAM,GAAG,cAAC,CAAC,AACV,gBAAgB,CAAE,IAAI,CAAC,CAAC,CAAC,IAAI,CAAC,CAAC,GAAG,CAAC;CACpC,CAAC,AACD,KAAK,cAAC,CAAC,AACN,QAAQ,KAAK,CACb,MAAM,CAAE,CAAC,CAAC,CAAC,CAAC,KAAK,CAAC,CAAC,CACnB,KAAK,CAAE,IAAI;CACZ,CAAC,AACD,QAAQ,cAAC,CAAC,AACT,KAAK,CAAE,IAAI,AACZ,CAAC,AACD,MAAM,cAAC,CAAC,AACP,MAAM,IAAI,CACV,KAAK,CAAE,IAAI,CACX,MAAM,CAAE,CAAC,CAAC,GAAG,CAAC,GAAG,CAAC,CAAC,AACpB,CAAC,AACD,QAAQ,cAAC,CAAC,AACT,KAAK,CAAE,IAAI,AACZ,CAAC\"}"
};

function mark_answers(question, markedKey) {
	if (markedKey == question.yes) return "yes"; else if (markedKey == question.no) return "no"; else return "";
}

const CreateTest = create_ssr_component(($$result, $$props, $$bindings, $$slots) => {
	let testname = "";
	let startKey;

	let questions = {
		1: {
			text: "What do you choose?",
			yes: 2,
			no: 3
		},
		2: {
			text: "See that if 'yes' was chosen. What's next?",
			yes: 4,
			no: ""
		},
		3: {
			text: "See that if 'no' was chosen. What's next?",
			yes: "",
			no: 5
		},
		4: {
			text: "Another 'yes'. What's next?",
			yes: "",
			no: ""
		},
		5: {
			text: "Another 'no'. What's next?",
			yes: "",
			no: ""
		},
		6: {
			text: "Wow and now you choose the opposite!",
			yes: "",
			no: ""
		}
	};

	let questionKey = "";

	// let text = '';
	// let yes = '';
	// let no = '';
	let i = 0;

	function reset_inputs(key, question) {
		questionKey = key;
	} // text = question.text;

	$$result.css.add(css);
	let readyTest = { name: testname, startKey, questions };

	let selectedQuestionKey = Object.keys(questions)[i];
	let selectedQuestion = questions[selectedQuestionKey];

	 {
		reset_inputs(selectedQuestionKey);
	}

	return `<div style="${"display: inline-block"}"><label><input placeholder="${"Test name"}" class="${"svelte-oj2ie5"}"${add_attribute("value", testname, 1)}></label>
	<p>Start question is </p>
	<select class="${"svelte-oj2ie5"}"${add_attribute("value", startKey, 1)}>${each(Object.entries(questions), ([key, question], i) => `<option${add_attribute("value", key, 0)}>${escape(key)}: ${escape(question.text)} </option>`)}</select></div>

<div><select${add_attribute("size", 10, 0)} class="${"svelte-oj2ie5"}"${add_attribute("value", i, 1)}>${each(Object.entries(questions), ([key, question], i) => `<option class="${escape(null_to_empty(mark_answers(selectedQuestion, key))) + " svelte-oj2ie5"}"${add_attribute("value", i, 0)}>(${escape(i)}) ${escape(key)} : ${escape(question.text)}
			</option>`)}</select></div>

<div><p>Question key = ${escape(questionKey)}</p>
	<label><textarea rows="${"4"}" placeholder="${"question text"}" class="${"svelte-oj2ie5"}">${selectedQuestion.text || ""}</textarea></label>
	<label><input placeholder="${"Next question key on 'yes'"}" class="${"svelte-oj2ie5"}"${add_attribute("value", selectedQuestion.yes, 1)}></label>
	<label><input placeholder="${"Next question key on 'no'"}" class="${"svelte-oj2ie5"}"${add_attribute("value", selectedQuestion.no, 1)}></label></div>



<div class="${"buttons svelte-oj2ie5"}"><button>Create</button>
	<button>Update</button>
	<button>Delete</button></div>

<p>To save test copy that JSON:<br>
	
	${escape(JSON.stringify(readyTest))}<br>
	and put it into store.js
</p>`;
});

const tests = writable([
    {
        "name":"FirstTest",
        "startKey":"1",
        "questions":{
            "1":{"text":"What do you choose?","yes":2,"no":3},
            "2":{"text":"See that if 'yes' was chosen. What's next?","yes":4,"no":""},
            "3":{"text":"See that if 'no' was chosen. What's next?","yes":"","no":5},
            "4":{"text":"Another 'yes'. What's next?","yes":"","no":""},
            "5":{"text":"Another 'no'. What's next?","yes":"","no":""},
            "6":{"text":"Wow and now you choose the opposite!","yes":"","no":""}
        }
    },
    {name: "EmptyTest", startKey:"", questions: {}}
]);

const test = writable(tests[0]);

/* src/ListTests.svelte generated by Svelte v3.21.0 */

const css$1 = {
	code: "select.svelte-1gj0lka{width:20em}button.svelte-1gj0lka{width:10em;margin-left:5em}",
	map: "{\"version\":3,\"file\":\"ListTests.svelte\",\"sources\":[\"ListTests.svelte\"],\"sourcesContent\":[\"<script>\\n    import { test, tests } from './tests.js'\\n    import { navigate } from \\\"svelte-routing\\\";\\n\\n    let selectedTest = $tests[0];\\n    \\n    function runTest(){\\n        test.set(selectedTest);\\n        navigate(\\\"/test\\\", {replace: true});\\n    }\\n</script>\\n\\n<style>\\n    select {\\n        width: 20em;\\n    }\\n\\n    button {\\n        width: 10em;\\n        margin-left: 5em;\\n    }\\n</style>\\n\\n<p>{JSON.stringify($test)}</p>\\n\\n<select bind:value={selectedTest} size={10}>\\n    {#each $tests as test}\\n        <option value={test}>{test.name}</option>\\n    {/each}\\n</select>\\n\\n<label><button on:click={runTest}>Run test</button></label>\\n\"],\"names\":[],\"mappings\":\"AAaI,MAAM,eAAC,CAAC,AACJ,KAAK,CAAE,IAAI,AACf,CAAC,AAED,MAAM,eAAC,CAAC,AACJ,KAAK,CAAE,IAAI,CACX,WAAW,CAAE,GAAG,AACpB,CAAC\"}"
};

const ListTests = create_ssr_component(($$result, $$props, $$bindings, $$slots) => {
	let $tests = get_store_value(tests);
	let $test = get_store_value(test);
	let selectedTest = $tests[0];

	$$result.css.add(css$1);

	return `<p>${escape(JSON.stringify($test))}</p>

<select${add_attribute("size", 10, 0)} class="${"svelte-1gj0lka"}"${add_attribute("value", selectedTest, 1)}>${each($tests, test => `<option${add_attribute("value", test, 0)}>${escape(test.name)}</option>`)}</select>

<label><button class="${"svelte-1gj0lka"}">Run test</button></label>`;
});

/* src/RunTest.svelte generated by Svelte v3.21.0 */

const css$2 = {
	code: "textarea.question.svelte-glk7yv{width:20em;height:7em}",
	map: "{\"version\":3,\"file\":\"RunTest.svelte\",\"sources\":[\"RunTest.svelte\"],\"sourcesContent\":[\"<script>\\n\\timport { test } from './tests.js'\\n\\n\\texport let firstQuestionKey = $test.startKey;\\n\\texport let questions = $test.questions;\\n\\tlet answers = [];\\n\\t\\n\\t\\n\\tlet questionKey = firstQuestionKey;\\n\\t$: question = questions[questionKey];\\n\\tlet i = 0;\\n\\t\\n\\tfunction next_question(keyName){\\n\\t\\tanswers = [...answers, {question, answer: keyName}]\\n\\t\\tquestionKey = question[keyName];//keyName can be 'yes' or 'no'\\t\\t\\t\\t\\n\\t}\\n\\t\\n\\tfunction upload_answer(){\\n\\t\\t\\n\\t\\treturn '';\\n\\t}\\n\\n</script>\\n\\n<style>\\n\\ttextarea.question {\\n\\t\\twidth: 20em;\\n\\t\\theight: 7em;\\n\\t}\\n</style>\\n\\n<!-- <p>{JSON.stringify(questions)}</p> -->\\n\\n{#if question}\\n<label><textarea class=\\\"question\\\">{question.text}</textarea></label>\\n\\n<button on:click=\\\"{()=>{next_question('yes')}}\\\">yes</button>\\n<button on:click=\\\"{()=>{next_question('no')}}\\\">no</button>\\n{:else}\\nFinished! {upload_answer()}\\n{/if}\\n\\n<hr>\\n\\n{#each answers as {question, answer}}\\n\\t<p>{question.text}, {answer}</p>\\n{/each}\\n\\n<!-- <p>\\n\\tDebug<br>\\n\\tCurrent question: {question.no}<br>\\n\\ti={i}\\n</p>\\n -->\"],\"names\":[],\"mappings\":\"AAyBC,QAAQ,SAAS,cAAC,CAAC,AAClB,KAAK,CAAE,IAAI,CACX,MAAM,CAAE,GAAG,AACZ,CAAC\"}"
};

function upload_answer() {
	return "";
}

const RunTest = create_ssr_component(($$result, $$props, $$bindings, $$slots) => {
	let $test = get_store_value(test);
	let { firstQuestionKey = $test.startKey } = $$props;
	let { questions = $test.questions } = $$props;
	let answers = [];
	let questionKey = firstQuestionKey;

	if ($$props.firstQuestionKey === void 0 && $$bindings.firstQuestionKey && firstQuestionKey !== void 0) $$bindings.firstQuestionKey(firstQuestionKey);
	if ($$props.questions === void 0 && $$bindings.questions && questions !== void 0) $$bindings.questions(questions);
	$$result.css.add(css$2);
	let question = questions[questionKey];

	return `

${question
	? `<label><textarea class="${"question svelte-glk7yv"}">${escape(question.text)}</textarea></label>

<button>yes</button>
<button>no</button>`
	: `Finished! ${escape(upload_answer())}`}

<hr>

${each(answers, ({ question, answer }) => `<p>${escape(question.text)}, ${escape(answer)}</p>`)}

`;
});

/* src/App.svelte generated by Svelte v3.21.0 */

const css$3 = {
	code: "nav.svelte-1v6pc6p{margin:2em 0 2em 0}",
	map: "{\"version\":3,\"file\":\"App.svelte\",\"sources\":[\"App.svelte\"],\"sourcesContent\":[\"<script>\\n  \\timport { Router, Link, Route } from \\\"svelte-routing\\\";\\n\\timport CreateTest from './CreateTest.svelte';\\n\\timport ListTests from './ListTests.svelte';\\n\\timport RunTest from './RunTest.svelte';\\n\\timport ShowTest from './ShowTest.svelte';\\n\\n\\t\\n\\n\\tlet url = \\\"\\\";\\n</script>\\n\\n<style>\\n\\tnav {\\n\\t\\tmargin: 2em 0 2em 0; \\n\\t}\\n</style>\\n\\n\\n<Router url=\\\"{url}\\\">\\n\\t<nav>\\n\\t\\t<Link to=\\\"/\\\">Tests</Link>\\n\\t\\t<Link to=\\\"create\\\">Create</Link>\\n\\t</nav>\\n\\t<div>\\n\\t\\t<Route path=\\\"/\\\" component=\\\"{ListTests}\\\" />\\n\\t\\t<Route path=\\\"create\\\" component=\\\"{CreateTest}\\\" />\\n\\t\\t<Route path=\\\"test\\\"> \\n\\t\\t\\t<RunTest/>\\n\\t\\t</Route>\\n\\t</div>\\n</Router>\\n\\n<!-- <CreateTest/> -->\\n<!-- <ListTests/> -->\\n<!-- <RunTest questions=\\\"{questions}\\\" firstQuestionKey={1}/> -->\\n<!-- <ShowTest/> -->\\n\"],\"names\":[],\"mappings\":\"AAaC,GAAG,eAAC,CAAC,AACJ,MAAM,CAAE,GAAG,CAAC,CAAC,CAAC,GAAG,CAAC,CAAC,AACpB,CAAC\"}"
};

let url = "";

const App = create_ssr_component(($$result, $$props, $$bindings, $$slots) => {
	$$result.css.add(css$3);

	return `${validate_component(Router, "Router").$$render($$result, { url }, {}, {
		default: () => `<nav class="${"svelte-1v6pc6p"}">${validate_component(Link, "Link").$$render($$result, { to: "/" }, {}, { default: () => `Tests` })}
		${validate_component(Link, "Link").$$render($$result, { to: "create" }, {}, { default: () => `Create` })}</nav>
	<div>${validate_component(Route, "Route").$$render($$result, { path: "/", component: ListTests }, {}, {})}
		${validate_component(Route, "Route").$$render($$result, { path: "create", component: CreateTest }, {}, {})}
		${validate_component(Route, "Route").$$render($$result, { path: "test" }, {}, {
			default: () => `${validate_component(RunTest, "RunTest").$$render($$result, {}, {}, {})}`
		})}</div>`
	})}




`;
});

module.exports = App;
