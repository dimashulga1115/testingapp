import { writable } from 'svelte/store';

export const tests = writable([
    {
        "name":"FirstTest",
        "startKey":"1",
        "questions":{
            "1":{"text":"What do you choose?","yes":2,"no":3},
            "2":{"text":"See that if 'yes' was chosen. What's next?","yes":4,"no":""},
            "3":{"text":"See that if 'no' was chosen. What's next?","yes":"","no":5},
            "4":{"text":"Another 'yes'. What's next?","yes":"","no":""},
            "5":{"text":"Another 'no'. What's next?","yes":"","no":""},
            "6":{"text":"Wow and now you choose the opposite!","yes":"","no":""}
        }
    },
    {name: "EmptyTest", startKey:"", questions: {}}
]);

export const test = writable($tests[0]);
